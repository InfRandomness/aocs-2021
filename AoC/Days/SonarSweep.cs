﻿using AoC.Utils;

namespace AoC.Days;

public class SonarSweep : IDay
{
    public static int ProcessIncremental(List<int> numbers)
    {
        int? number = null;
        var count = 0;
        foreach (var t in numbers)
        {
            if (number < t)
            {
                count++;
            }

            number = t;
        }

        return count;
    }

    public static int ProcessIncrementalPartTwo(List<int> numbers)
    {
        int? number = null;
        var count = 0;

        for (var i = 0; i < (numbers.Count - 2); i++)
        {
            if (numbers.Count <= i + 1 && numbers.Count <= i + 2) continue;

            if (number < numbers[i] + numbers[i + 1] + numbers[i + 2])
            {
                count++;
            }
            
            number = numbers[i] + numbers[i + 1] + numbers[i + 2];
        }

        return count;
    }

    private static void SolvePartOne(List<int> content)
    {
        Console.WriteLine("=== Part One ===");
        Console.WriteLine("Number of increments : {0}", ProcessIncremental(content));
    }

    private static void SolvePartTwo(List<int> content)
    {
        Console.WriteLine("=== Part Two ===");
        Console.WriteLine("Number of increments : {0}", ProcessIncrementalPartTwo(content));
    }

    public void Run(string[] arguments)
    {
        var content = Io.ReadFileIntoIntList("day1");
        SolvePartOne(content);
        SolvePartTwo(content);
    }
}