﻿using AoC.Utils;

namespace AoC.Days;

public class Submarine
{
    internal int HorizontalPosition;
    internal int Depth;
    internal int Aim;

    public Submarine(int horizontalPosition, int depth)
    {
        HorizontalPosition = horizontalPosition;
        Depth = depth;
    }

    public Submarine(int horizontalPosition, int depth, int aim)
    {
        HorizontalPosition = horizontalPosition;
        Depth = depth;
        Aim = aim;
    }

    //Removal
    // internal void IncrementDepth(int value)
    // {
    //     Depth += value;
    // }
    //
    // internal void DecrementDepth(int value)
    // {
    //     Depth -= value;
    // }
    //
    // internal void UpdateHorizontalPosition(int value)
    // {
    //     HorizontalPosition += value;
    // }
}

public class Dive : IDay
{
    public enum Instruction
    {
        Forward,
        Up,
        Down,
    }

    public struct InstructionData
    {
        public Instruction type;
        public int value;

        public InstructionData(Instruction type, int value)
        {
            this.type = type;
            this.value = value;
        }
    }

    public static List<InstructionData> Parse(string content)
    {
        var stringReader = new StringReader(content);
        var instructions = new List<InstructionData>();
        while (true)
        {
            var line = stringReader.ReadLine();
            var splitLine = line?.Split(" ");
            if (line == null)
            {
                break;
            }
            switch (splitLine?[0])
            {
                case "forward":
                    instructions.Add(new InstructionData(Instruction.Forward, int.Parse(splitLine[1])));
                    break;
                case "up":
                    instructions.Add(new InstructionData(Instruction.Up, int.Parse(splitLine[1])));
                    break;
                case "down":
                    instructions.Add(new InstructionData(Instruction.Down, int.Parse(splitLine[1])));
                    break;
            }
        }

        return instructions;
    }

    private static Submarine ApplyCoursePartOne(IEnumerable<InstructionData> instructions)
    {
        var horizontalPosition = 0;
        var depth = 0;
        foreach (var instruction in instructions)
        {
            switch (instruction.type)
            {
                case Instruction.Up:
                    depth -= instruction.value;
                    break;
                case Instruction.Down:
                    depth += instruction.value;
                    break;
                case Instruction.Forward:
                    horizontalPosition += instruction.value;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        return new Submarine(horizontalPosition, depth);
    }

    private static Submarine ApplyCoursePartTwo(IEnumerable<InstructionData> instructions)
    {
        var horizontalPosition = 0;
        var depth = 0;
        var aim = 0;
        
        foreach (var instruction in instructions)
        {
            switch (instruction.type)
            {
                case Instruction.Up:
                    aim -= instruction.value;
                    break;
                case Instruction.Down:
                    aim += instruction.value;
                    break;
                case Instruction.Forward:
                    // Console.WriteLine("Aim : {0}", aim);
                    // Console.WriteLine("horizotal position : {0}", horizontalPosition);
                    // Console.WriteLine("depth : {0}", depth);
                    // Console.WriteLine("instruction value : {0}", instruction.value);
                    horizontalPosition += instruction.value;
                    depth += (aim * instruction.value);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        return new Submarine(horizontalPosition, depth, aim);
    }
    
    private static void SolvePartOne(IEnumerable<InstructionData> content)
    {
        var result = ApplyCoursePartOne(content);
        Console.WriteLine("=== Part One ===");
        Console.WriteLine("Depth : {0}\nHorizontal Position : {1}", result.Depth, result.HorizontalPosition);
        Console.WriteLine("Horizontal Position * Depth = {0}", result.HorizontalPosition * result.Depth);
    }

    private static void SolvePartTwo(IEnumerable<InstructionData> content)
    {
        var result = ApplyCoursePartTwo(content);
        Console.WriteLine("=== Part Two ===");
        Console.WriteLine("Depth : {0}\nHorizontal Position : {1}\nAim : {2}", result.Depth, result.HorizontalPosition, result.Aim);
        Console.WriteLine("Horizontal Position * Depth = {0}", result.HorizontalPosition*result.Depth);
    }

    public void Run(string[] arguments)
    {
        var input = File.ReadAllText(Path.Combine(Aoc.InputPath + "day2"));
        var instructions = Parse(input);
        SolvePartOne(instructions);
        SolvePartTwo(instructions);
    }
}