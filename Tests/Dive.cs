﻿using System;
using System.Collections.Generic;
using Xunit;

namespace Tests;

public class Dive
{
    [Fact]
    public void TestParser()
    {
        var instructions = "forward 10\rdown 10\rup 15";
        var instructionsList = new List<AoC.Days.Dive.InstructionData>
        {
            new AoC.Days.Dive.InstructionData(AoC.Days.Dive.Instruction.Forward, 10),
            new AoC.Days.Dive.InstructionData(AoC.Days.Dive.Instruction.Down, 10),
            new AoC.Days.Dive.InstructionData(AoC.Days.Dive.Instruction.Up, 15),
        };
        var parsedData = AoC.Days.Dive.Parse(instructions);
        Assert.Equal(parsedData, instructionsList);
    }
}