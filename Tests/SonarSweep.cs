using System.Collections.Generic;
using Xunit;

namespace Tests;

public class SonarSweep
{
    [Fact]
    public void TestProcessIncrementalPartOne()
    {
        var numbers = new List<int> {1, 2, 3, 1, 4, 5, 1};
        Assert.Equal(4, AoC.Days.SonarSweep.ProcessIncremental(numbers));
    }

    [Fact]
    public void TestProcessIncrementalPartTwo()
    {
        var numbers = new List<int> {156, 153, 163, 168, 166, 164, 149, 187, 190};
        Assert.Equal(5, AoC.Days.SonarSweep.ProcessIncrementalPartTwo(numbers));
    }
}